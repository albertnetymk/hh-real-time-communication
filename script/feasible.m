function [result] = feasible(T)
% T [m x 5]
% [Index C T D Jitter]


% Is it necessary to check U as well?
% if (sum(T(:,2)./T(:,3))> 1)
%     result = 0;
%     return;
% end

L_a = max([T(:,4)' ...
    dot(T(:,3)-T(:,4), T(:,2)./T(:,3))/(1-sum(T(:,2)./T(:,3)))]);
L_b = busy_period(T);
t_max = min(L_a, L_b);
S = [];
for i = 1:size(T, 1)
	j = 0;
	while (1)
		d_k = j*T(i,3) + T(i,4);
		if (d_k <= t_max)
			S(1, size(S, 2)+1) = d_k;
		else
			break;
		end
		j = j+1;
	end
end
S = unique(S);
for i = 1:size(S, 2)
    h_i = floor((S(i) + T(:,3) - T(:,4))./T(:,3)).*T(:,2);
% 	h = sum(h_i(h_i>0));
   	h = sum(h_i);
	if (h > S(i))
		result = 0;
		return
	end
end
result = 1;
end

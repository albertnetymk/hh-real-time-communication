function [result] = busy_period(T)
% T [m x 5]
% [Index C T D Jitter]

L_prev = sum(T(:,2));
i = 0;
while(1)
	L_curr = dot(ceil(L_prev ./ T(:,3)), T(:,2));
    i = i+1;
	if (L_curr == L_prev)
		break;
	end
	L_prev = L_curr;
end
result = L_curr;
end

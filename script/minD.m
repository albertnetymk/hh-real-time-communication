function [result] = minD(T, x)
T(x,4) = T(x,2);
L_a = max([T(:,4)' ...
    dot(T(:,3)-T(:,4), T(:,2)./T(:,3))/(1-sum(T(:,2)./T(:,3)))]);
L_b = busy_period(T);
t_max = min(L_a, L_b);
while(1)
    S = [];
    for i = 1:size(T, 1)
        j = 0;
        while (1)
            d_k = j*T(i,3) + T(i,4);
            if (d_k <= t_max)
                S(1, size(S, 2)+1) = d_k;
            else
                break;
            end
            j = j+1;
        end
    end
    S = unique(S);
    for i = 1:size(S, 2)
        h = sum(floor((S(i) + T(:,3) - T(:,4))./T(:,3)).*T(:,2));
        if (h > S(i))
            K = ceil((h-S(i))/T(x,2));
            r = floor(S(i)/T(x,3))*T(x,3);
            T(x,4) = h + (K-1)*(T(x,3) - T(x,2)) -r;
            i = 0;
            break;
        end
    end
    if (i == size(S,2))
        break;
    end
    if (i == 0)
        continue;
    end
end
result = T(x,4);
S
end

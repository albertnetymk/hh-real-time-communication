# Real-Time Networking 2012-04 -- 2012-06
Some information on scheduling and communication protocols. Completely new domain from my past experience, and I don't think I will pursue my career
in this field.

Learned something by implementing the *minD* algorithm from "Computing the Minimum EDF Feasible Deadline in Periodic Systems". Total Bandwidth Server
(TBS) should also but implemented but it's not covered due to the fact I don't have a partner working on the same topic.

* `busy_period.m` # calculate busy period
* `feasible.m`    # determine if the tasks set is feasible under EDF
* `minD.m`        # minD algorithm
* `main.m`

PS: This is the only course I had to take re-exam to pass, twice!!
